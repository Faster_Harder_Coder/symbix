#include <e32base.h>
#include <e32cons.h>

void MainL();

GLDEF_C TInt E32Main(void)
{
    CTrapCleanup *cleanup=CTrapCleanup::New();
    if(cleanup==NULL)
    {
        _LIT(KCleanupPanic,"Cleanup Stack");
        User::Panic(KCleanupPanic,666);
    }
    TRAPD(err,MainL());
    if(err!=KErrNone)
    {
        _LIT(KMainPanic,"MainL");
        User::Panic(KMainPanic,err);
    }
    delete cleanup;
    return 0;
}

void MainL()
{
    CConsoleBase *console=Console::NewL(KNullDesC,TSize(KConsFullScreen,KConsFullScreen));
    _LIT(KHelloWorld,"HelloWorld\n");
    console->Printf(KHelloWorld);
    console->Getch();
    delete console;
}